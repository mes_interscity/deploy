### What is this?

This repository contains the ansible playbooks to deploy this group's projects in Docker containers.

You can follow the discussions on these playbooks development [here](https://gitlab.com/smart-city-platform/resources-catalog/issues/18)

If you have write permissions to this repository, feel free to push your patches here.

##### Things we need:

* Playbooks to pull a single repository and rebuild only the respective container
* More test cases like [this](https://gitlab.com/smart-city-software-platform/resource-cataloguer/blob/master/scripts/try_api.sh), covering each feature of each single application (start with the core ones)
* A shunit2 test suite based on the test cases
* a playbook to run these shunit2 testcases
* Set host firewall (through ansible) in case we want to deploy this somewhere else
* Remove redundancies on docker-host role templates

##### How to get access?

Open an issue in this repository if you need access to revoada.

##### I want to deploy it myself

You can do it locally using either your machine or a VM (vagrantfile provided) as a host.

If you have a VPS which you want to share with the group, let us know (open an issue here) and we will help you deploying the whole thing there.

##### How to deploy

Just run site.yml playbook. to properly start data_collector, you also need to run start-collector-threads.yml after site.yml. In order to seed and register components, you also want to run playbooks/set-components.yml after that.

